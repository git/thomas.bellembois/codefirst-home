module codefirst.iut.uca.fr/tbellembois/codefirst-home

go 1.19

require (
	github.com/BurntSushi/toml v1.2.0
	github.com/maragudk/gomponents v0.18.0
	github.com/nicksnyder/go-i18n/v2 v2.2.0
	golang.org/x/text v0.3.7
)

require gopkg.in/yaml.v2 v2.4.0 // indirect
