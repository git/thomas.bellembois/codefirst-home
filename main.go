package main

import (
	"embed"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/BurntSushi/toml"

	g "github.com/maragudk/gomponents"
	c "github.com/maragudk/gomponents/components"
	. "github.com/maragudk/gomponents/html"

	"golang.org/x/text/language"

	"github.com/nicksnyder/go-i18n/v2/i18n"
)

var (
	//go:embed web/*
	embedWebBox embed.FS

	i18nBundle *i18n.Bundle

	test                      bool
	codefirsturl, registryurl string
	proxypath                 string
)

func init() {
	i18nBundle = i18n.NewBundle(language.English)
	i18nBundle.RegisterUnmarshalFunc("toml", toml.Unmarshal)
	i18nBundle.MustLoadMessageFile("lang/en.toml")
	i18nBundle.MustLoadMessageFile("lang/fr.toml")

	flag.StringVar(&codefirsturl, "codefirsturl", "", "code#0 main URL")
	flag.StringVar(&registryurl, "registryurl", "", "code#0 registry URL")
	flag.StringVar(&proxypath, "proxypath", "/home/", "application proxy path (default: /home)")
	flag.BoolVar(&test, "test", false, "render the page for development purposes")
	flag.Parse()

	if !test && (codefirsturl == "" || registryurl == "") {
		fmt.Println("Missing parameter.")
		os.Exit(1)
	}

	if test {
		proxypath = "/"
	}

	codefirsturl = strings.TrimSuffix(codefirsturl, "/")
	registryurl = strings.TrimSuffix(registryurl, "/")
}

func handler(w http.ResponseWriter, r *http.Request) {
	xForwardedUser := r.Header["X-Forwarded-User"]

	if test {
		xForwardedUser = []string{"test@test.com"}
	}

	if len(xForwardedUser) == 0 || xForwardedUser[0] == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	lang := r.FormValue("lang")
	accept := r.Header.Get("Accept-Language")
	localizer := i18n.NewLocalizer(i18nBundle, lang, accept)

	// changePasswordMessage := localizer.MustLocalize(&i18n.LocalizeConfig{
	// 	DefaultMessage: &i18n.Message{
	// 		ID: "changePassword",
	// 	},
	// })
	logoutMessage := localizer.MustLocalize(&i18n.LocalizeConfig{
		DefaultMessage: &i18n.Message{
			ID: "logout",
		},
	})
	projectsMessage := localizer.MustLocalize(&i18n.LocalizeConfig{
		DefaultMessage: &i18n.Message{
			ID: "projects",
		},
	})
	// cicdMessage := localizer.MustLocalize(&i18n.LocalizeConfig{
	// 	DefaultMessage: &i18n.Message{
	// 		ID: "cicd",
	// 	},
	// })
	// codeAnalyseMessage := localizer.MustLocalize(&i18n.LocalizeConfig{
	// 	DefaultMessage: &i18n.Message{
	// 		ID: "codeAnalyse",
	// 	},
	// })
	// registryMessage := localizer.MustLocalize(&i18n.LocalizeConfig{
	// 	DefaultMessage: &i18n.Message{
	// 		ID: "registry",
	// 	},
	// })
	// apiMessage := localizer.MustLocalize(&i18n.LocalizeConfig{
	// 	DefaultMessage: &i18n.Message{
	// 		ID: "api",
	// 	},
	// })
	// documentationMessage := localizer.MustLocalize(&i18n.LocalizeConfig{
	// 	DefaultMessage: &i18n.Message{
	// 		ID: "documentation",
	// 	},
	// })
	// chatMessage := localizer.MustLocalize(&i18n.LocalizeConfig{
	// 	DefaultMessage: &i18n.Message{
	// 		ID: "chat",
	// 	},
	// })

	pageContent := Div(Class("card mx-auto mt-5 p-3"), StyleAttr("width: 29rem;"),
		Img(Src(proxypath+"web/img/logo.png"), Width("110px"), Class("mx-auto d-block")),
		Div(Class("card-body"),
			P(Class("card-text text-center fst-italic"), g.Text(xForwardedUser[0])),
			Div(Class("row"),
				A(Href(fmt.Sprintf("%s/git/user/oauth2/keycloak", codefirsturl)), Span(Class("mdi mdi-24px mdi-git"), g.Text(projectsMessage))),
			),
			Div(Class("row"),
				A(Href(fmt.Sprintf("%s/keycloak/realms/master/protocol/openid-connect/logout", codefirsturl)), Span(Class("mdi mdi-24px mdi-logout"), g.Text(logoutMessage))),
			),
			// Div(Class("row"),
			// 	A(Href(fmt.Sprintf("%s/keycloak/realms/master/account/?referrer=security-admin-console&referrer_uri=%s/keycloak/auth/admin/master/console/%23%2Fforbidden#/security/signingin", codefirsturl, codefirsturl)), Span(Class("mdi mdi-24px mdi-lock-reset"), g.Text(changePasswordMessage))),
			// ),
			// Div(Class("row"),
			// 	A(Href(fmt.Sprintf("%s/login", codefirsturl)), Span(Class("mdi mdi-24px mdi-flash"), g.Text(cicdMessage))),
			// ),
			// Div(Class("row"),
			// 	A(Href(fmt.Sprintf("%s/sonar/sessions/init/oidc?return_to=/sonar/", codefirsturl)), Span(Class("mdi mdi-24px mdi-radar"), g.Text(codeAnalyseMessage))),
			// ),
			// Div(Class("row"),
			// 	A(Href(registryurl), Span(Class("mdi mdi-24px mdi-docker"), g.Text(registryMessage))),
			// ),
			// Div(Class("row"),
			// 	A(Href(fmt.Sprintf("%s/cinny/", codefirsturl)), Span(Class("mdi mdi-24px mdi-wechat"), g.Text(chatMessage))),
			// ),
			// Div(Class("row"),
			// 	A(Href(fmt.Sprintf("%s/dockerrunner/static/html/", codefirsturl)), Span(Class("mdi mdi-24px mdi-api"), g.Text(apiMessage))),
			// ),
			// Div(Class("row"),
			// 	A(Href(fmt.Sprintf("%s/documentation/", codefirsturl)), Span(Class("mdi mdi-24px mdi-file-document-edit-outline"), g.Text(documentationMessage))),
			// ),
		),
		Div(Class("mt-sm-4"),
			Img(Src(proxypath+"web/img/logo_UCA.png"), Height("50px"), Class("m-sm-2 float-start mx-auto d-block")),
			Img(Src(proxypath+"web/img/logo_I-SITE.png"), Height("50px"), Class("m-sm-2 float-start mx-auto d-block")),
			Img(Src(proxypath+"web/img/logo_LIA.png"), Height("50px"), Class("m-sm-2 float-start mx-auto d-block")),
			Img(Src(proxypath+"web/img/logo_investir.png"), Height("50px"), Class("m-sm-2 float-start mx-auto d-block"))),
	)

	page := c.HTML5(c.HTML5Props{
		Title:    "Code#0",
		Language: "fr",
		Head: []g.Node{
			Link(Rel("stylesheet"), Href(proxypath+"web/css/materialdesignicons.min.css")),
			Link(Rel("stylesheet"), Href(proxypath+"web/css/bootstrap.min.css")),
			StyleEl(Type("text/css"), g.Raw(`
			body {
				height: 100%;
				background-image: url('`+proxypath+`web/img/home_background.png');
				background-position: center;
				background-repeat-x: no-repeat;
				background-repeat-y: repeat;
				background-size: cover;
			}
			`)),
		},
		Body: []g.Node{
			Div(Class("container"), ID("indexPage"), pageContent),
			Div(g.Group([]g.Node{
				Script(Src(proxypath + "web/js/bootstrap.bundle.min.js")),
				g.Raw(`
				<script>
				</script>
				`),
			}),
			),
		},
	})

	if err := page.Render(w); err != nil {
		panic(err)
	}
}

func main() {
	http.Handle("/web/", http.FileServer(http.FS(embedWebBox)))
	http.HandleFunc("/", handler)

	log.Println("Server running.")
	log.Fatal(http.ListenAndServe(":8081", nil))
}
