#!/usr/bin/env bash

if [ -z "$CODEFIRSTURL" ] || [ -z "$REGISTRYURL" ]
then
    echo "Missing parameter."
    exit 1
fi

/go/bin/codefirst-home -codefirsturl=$CODEFIRSTURL -registryurl=$REGISTRYURL