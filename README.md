# Code#0 home page

Background generated with:

```bash
grep -v "///" /tmp/TarotController.cs | tr -d "\n" > /tmp/text.txt
sed -i 's/  */ /g' text.txt

convert -background "#000000" -fill "#ffffff" -font "B612-Mono-Regular" -pointsize 14 -blur 0x2 -size 1280x1024 caption:@/tmp/text.txt /tmp/output.png
```